import { __dirname, pluginNameEng } from "../components/Path.js";
import { Version } from "../components/index.js";
let BotName = Version.isTrss
  ? "Trss-Yunzai"
  : Version.isMiao
    ? "Miao-Yunzai"
    : "Yunzai-Bot";
export const helpCfg = {
  themeSet: false,
  title: `${pluginNameEng}-HelpList`,
  subTitle: `${BotName} & ${pluginNameEng}`,
  colWidth: 265,
  theme: "all",
  themeExclude: ["default"],
  colCount: 3,
  bgBlur: true,
};

export const helpList = [
  {
    group: "实用工具",
    list: [
      {
        icon: 12,
        title: "#一键群发",
        desc: "发送消息到多个群组,后面接内容",
      },
      {
        icon: 12,
        title: "#一键私发",
        desc: "发送消息到多个私聊,后面接内容",
      },
      {
        icon: 12,
        title: "#一键打卡",
        desc: "一键群打卡",
      },
      {
        icon: 12,
        title: "#开启/关闭一键打卡",
        desc: "开启或关闭一键打卡功能",
      },
      {
        icon: 12,
        title: "#取",
        desc: "取消息内容,带pb",
      },
      {
        icon: 12,
        title: "#ad安装插件",
        desc: "安装插件",
      },
      {
        icon: 12,
        title: "#ad加密帮助",
        desc: "实用加解密",
      },
      {
        icon: 13,
        title: "#开启/关闭本群消息传递",
        desc: "把一个群的消息传递到另一个群",
      },
      {
        icon: 13,
        title: "#查找",
        desc: "查找某个或者一群人与Bot的共同群,例如#查找@1 @2 @3或#查找123 123 123",
      },
      {
        icon: 13,
        title: "#查找好友",
        desc: "查找某个群中是否有人与Bot有好友,例如#查找好友123 123 123",
      },
      {
        icon: 13,
        title: "#xxx寻人",
        desc: "xxx是加密方法,比如#md5 md5_up base64寻人xxxxx,返回找到的人的QQ,操作是从左到右开始的",
      },
    ],
  },
  {
    group: "API",
    list: [
      {
        icon: 29,
        title: "#ad视频列表",
        desc: "视频api~",
      },
      {
        icon: 29,
        title: "#随机loli",
        desc: "不是萝莉,指令后面可以接想要搜的图的标签,空格分隔",
      },
    ],
  },
  {
    group: "一键跑路功能",
    list: [
      {
        icon: 30,
        title: "#获取群员名单",
        desc: "字面意思",
      },
      {
        icon: 30,
        title: "#保存群员名单",
        desc: "保存于插件目录/data/groupmember/",
      },
      {
        icon: 30,
        title: "#本地群员名单",
        desc: "读取名单",
      },
    ],
  },
];
