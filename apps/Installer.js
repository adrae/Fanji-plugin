import { Restart } from '../../other/restart.js'
import fs from 'fs'
import { exec } from 'child_process'
import Base from '../model/base/Base.js'
let isInstalling = false

const pluginList = {
  'reset-qianyu-plugin':
    'https://gitee.com/think-first-sxs/reset-qianyu-plugin',
  'ad-plugin': 'https://gitee.com/adrae/ad-plugin',
  'cunyx-plugin': 'https://gitee.com/cunyx/cunyx-plugin',
  'TRSS-Plugin': 'https://Yunzai.TRSS.me',
  'useless-plugin': 'https://gitee.com/SmallK111407/useless-plugin',
  'StarRail-plugin': 'https://gitee.com/hewang1an/StarRail-plugin',
  'xiaoyao-cvs-plugin': 'https://gitee.com/Ctrlcvs/xiaoyao-cvs-plugin',
  'Circle-money-run-plugin':
    'https://gitee.com/theqingyao/Circle-money-run-plugin',
  'fengye-plugin': 'https://gitee.com/maple-leaf-sweeping/fengye-plugin',
  'xiaoye-plugin': 'https://gitee.com/xiaoye12123/xiaoye-plugin',
  'ws-plugin': 'https://gitee.com/xiaoye12123/ws-plugin',
  'Shiranai-Plugin': 'https://github.com/XasYer/Shiranai-Plugin'
}
const names = {
  'reset-qianyu-plugin': '千羽插件',
  'ad-plugin': 'ad插件',
  'cunyx-plugin': '寸幼萱插件',
  'TRSS-Plugin': 'TRSS插件',
  'useless-plugin': '无用插件',
  'StarRail-plugin': '星铁插件',
  'xiaoyao-cvs-plugin': '逍遥插件',
  'Circle-money-run-plugin': '跑路插件',
  'fengye-plugin': '枫叶插件-与现存枫叶无关',
  'xiaoye-plugin': '小叶插件',
  'ws-plugin': 'ws插件',
  'Shiranai-Plugin': '希腊奶插件'
}
const mergedPlugins = Object.keys(pluginList).reduce((acc, name) => {
  acc[name] = {
    url: pluginList[name],
    ChineseName: names[name] || name
  }
  return acc
}, {})
class PluginInstaller {
  async install (e, name, url, path, forceInstall) {
    e.reply(`开始安装 ${mergedPlugins[name].ChineseName} `)

    if (forceInstall) {
      let folderExists = false
      try {
        await fs.promises.access(path)
        folderExists = true
      } catch (error) {
        folderExists = false
      }

      if (folderExists) {
        try {
          await fs.promises.rm(path, { recursive: true })
          e.reply('删除原先文件夹成功')
        } catch (error) {
          e.reply(`删除原先文件夹失败：${error.toString()}`)
          return false
        }
      }
    }

    const cmd = `git clone --depth 1 --single-branch "${url}" "${path}"`
    isInstalling = true
    await exec(cmd, async (error, stdout, stderr) => {
      isInstalling = false
      if (error) {
        e.reply(`安装失败！\n错误信息：${error.toString()}\n${stderr}`)
        return false
      }

      await exec(
        'pnpm install',
        { cwd: path },
        async (error, stdout, stderr) => {
          if (error) {
            e.reply(`安装失败！\n错误信息：${error.toString()}\n${stderr}`)
            return false
          }
          e.reply(`${mergedPlugins[name].ChineseName} 插件安装成功`)

          new Restart(e).restart()
          return true
        }
      )
    })
  }
}

const installer = new PluginInstaller()

export class PluginInstall extends Base {
  constructor () {
    super({
      name: 'ad[安装插件]',
      dsc: 'ad安装插件',
      priority: -1,
      rule: [
        {
          reg: /^#ad(强制)?安装(插件|${Object.keys(pluginList).join("|")})$/i,
          fnc: 'install'
        }
      ]
    })
  }

  async install (e) {
    if (!(e.isMaster || (await this.cm.check(e.user_id)))) { return await e.reply('你没有权限') }

    if (isInstalling) {
      await e.reply('已有命令安装中，请勿重复操作')
      return false
    }
    if (!e.atme && e.at) return false
    const forceInstall = e.msg.includes('强制')
    const pluginName = e.msg.replace(/^#ad(强制)?安装/i, '').trim()
    if (pluginName === '插件') {
      let msg = '\n'
      const checkPromises = Object.keys(pluginList).map(async (pluginsName) => {
        try {
          await fs.promises.access(`plugins/${pluginsName}`)
          logger.info(
            `${pluginsName}（${mergedPlugins[pluginsName].ChineseName}）存在`
          )
        } catch (error) {
          if (error.code === 'ENOENT') {
            msg += `${pluginsName}\n(${mergedPlugins[pluginsName].ChineseName})\n`
          }
        }
        return true
      })
      await Promise.all(checkPromises)

      if (msg === '\n') {
        msg = '暂无可安装插件'
      } else {
        msg = `可安装插件列表：${msg}发送 #ad安装+插件名 进行安装`
      }
      await e.reply(msg)
      return true
    }

    const pluginPath = `plugins/${pluginName}`
    try {
      await fs.promises.access(pluginPath)
      await e.reply(
        `${pluginName}(${mergedPlugins[pluginsName].ChineseName}) 已安装`
      )
      return false
    } catch (error) {
      if (error.code !== 'ENOENT') {
        throw error
      }
    }

    await installer.install(
      e,
      pluginName,
      pluginList[pluginName],
      pluginPath,
      forceInstall
    )
    return true
  }
}
