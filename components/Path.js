import { fileURLToPath } from 'url'
import path from 'path'
const Path = process.cwd()
const __dirname =
  path.resolve(path.dirname(fileURLToPath(import.meta.url)), '..') + '/'
const pluginName = 'Ad插件'
const pluginNameEng = 'ad-plugin'
const Plugin_Path = path.join(Path, 'plugins', pluginNameEng)
export { __dirname, pluginName, pluginNameEng, Plugin_Path }
