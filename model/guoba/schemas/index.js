import lodash from 'lodash'
import api from './api.js'

import Config from '../../../model/base/Config.js'

export const schemas = [
    api
].flat()

export function getConfigData () {
  return {
    api: Config.GetCfg('api')
  }
}

export async function setConfigData (data, { Result }) {
  for (let key in data) {
    await Config.SetCfg(...key.split("."), data[key])
  }
  return Result.ok({}, '𝑪𝒊𝒂𝒍𝒍𝒐～(∠・ω< )⌒★')
}
