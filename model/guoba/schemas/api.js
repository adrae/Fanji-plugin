export default [
    {
        label: 'API服务',
        component: 'SOFT_GROUP_BEGIN'
    },
    {
        component: 'Divider',
        label: 'API服务端设置'
    },
    {
        field: 'api.port',
        label: 'API端口',
        bottomHelpMessage: 'API服务监听的端口，默认80',
        component: 'InputNumber'
    }
]