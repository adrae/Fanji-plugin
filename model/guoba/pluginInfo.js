import { PluginPath } from '#components'

export default {
    name: 'ad-plugin',
    title: 'Ad插件',
    author: '@Admilk',
    authorLink: 'https://gitee.com/adrae',
    link: 'https://gitee.com/adrae/ad-plugin',
    isV3: true,
    isV2: false,
    description: '主要提供清凉图api功能',
    // 显示图标，此为个性化配置
    // 图标可在 https://icon-sets.iconify.design 这里进行搜索
    icon: 'svg-spinners:blocks-scale',
    // 图标颜色，例：#FF0000 或 rgb(255, 0, 0)
    iconColor: '#d19f56'
    // 如果想要显示成图片，也可以填写图标路径（绝对路径）
    // iconPath: path.join(_paths.pluginRoot, 'resources/images/icon.png'),
  }
