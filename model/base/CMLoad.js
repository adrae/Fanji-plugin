import { __dirname } from '../../components/Path.js'
import path from 'path'
import fs from 'fs/promises'
async function loadCM () {
  const libPath = path.resolve(__dirname, '../../../../lib/common/CM.js')
  const pluginPath = path.resolve(__dirname, './lib/common/CM.js')

  try {
    await fs.access(libPath, fs.constants.F_OK)
    return (await import('file://' + libPath)).default
  } catch {
    try {
      await fs.access(pluginPath, fs.constants.F_OK)
      return (await import('file://' + pluginPath)).default
    } catch (error) {
      logger.error('加载 CM.js 失败:', error.message)
      throw error
    }
  }
}
export const cm = await loadCM()
