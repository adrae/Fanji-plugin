// import { LookupAddress } from "dns";
import { lookup } from 'dns/promises'

/**
 * 判断给定的 hostname 或 IP 是否为本机的回环地址 支持v6(可能不全)
 * @param hostnameOrIp - 要检查的 hostname 或 IP 地址
 */
export const isLoopback = async (hostnameOrIp) => {
  try {
    const addresses = await lookup(hostnameOrIp, { all: true })
    return addresses.some(
      (addr) => isIPv4Loop(addr.address) || isIPv6Loop(addr.address)
    )
  } catch {
    // 如果解析失败 则默认不是回环地址
    return false
  }
}

/**
 * 判断一个 v4 地址是否在回环范围内
 * @param ip - IPv4 地址字符串
 */
export const isIPv4Loop = (ip) => {
  const parts = ip.split('.').map(Number)
  const isLoopback = parts.length === 4 && parts[0] === 127
  const isInRange = parts.every(
    (part) => Number.isInteger(part) && part >= 0 && part <= 255
  )
  return isLoopback && isInRange
}

/**
 * 判断一个 IPv6 地址是否是回环地址 (::1 或 ::ffff:127.x.x.x)
 * @param ip - IPv6 地址字符串
 */
export const isIPv6Loop = (ip) => {
  if (ip === '::1') return true
  if (ip.startsWith('::ffff:')) {
    const ipv4Part = ip.substring(7)
    return isIPv4Loop(ipv4Part)
  }
  return false
}

/**
 * 获取请求的 IP 地址
 * 优先级: x-forwarded-for > remoteAddress > ip > hostname
 * @param req - 请求对象
 */
export const getRequestIp = (req) => {
  const list = []

  /** 反向代理的 IP */
  const xForwardedFor = req.headers['x-forwarded-for']
  if (Array.isArray(xForwardedFor)) {
    list.push(...xForwardedFor)
  } else if (typeof xForwardedFor === 'string') {
    list.push(xForwardedFor)
  }

  const remoteAddress = req.socket.remoteAddress
  const ip = req.ip
  const hostname = req.hostname

  if (ip) list.push(ip)
  if (hostname) list.push(hostname)
  if (remoteAddress) list.push(remoteAddress)

  return list.filter(Boolean)
}

/**
 * 传入一个请求对象 判断是否为本机请求
 * @param req - 请求对象
 */
export const isLocalRequest = async (req) => {
  const ips = getRequestIp(req)
  for (const ip of ips) {
    /** 如果有一个不是本机地址 则返回 false */
    const isLocal = await isLoopback(ip)
    if (!isLocal) return false
  }
  return true
}
class Color {
  static colors = {
    reset: '\u001b[0m',
    black: '\u001b[30m',
    red: '\u001b[31m',
    green: '\u001b[32m',
    yellow: '\u001b[33m',
    blue: '\u001b[34m',
    magenta: '\u001b[35m',
    cyan: '\u001b[36m',
    white: '\u001b[37m',
    gray: '\u001b[90m',
    brightRed: '\u001b[91m',
    brightGreen: '\u001b[92m',
    brightYellow: '\u001b[93m',
    brightBlue: '\u001b[94m',
    brightMagenta: '\u001b[95m',
    brightCyan: '\u001b[96m',
    brightWhite: '\u001b[97m'
  }

  static apply (color, text) {
    const colorCode = this.colors[color]
    return colorCode ? `${colorCode}${text}${this.colors.reset}` : text
  }

  // 各种颜色方法
  static red (text) {
    return this.apply('red', text)
  }

  static green (text) {
    return this.apply('green', text)
  }

  static yellow (text) {
    return this.apply('yellow', text)
  }

  static blue (text) {
    return this.apply('blue', text)
  }

  static magenta (text) {
    return this.apply('magenta', text)
  }

  static cyan (text) {
    return this.apply('cyan', text)
  }

  static white (text) {
    return this.apply('white', text)
  }

  static gray (text) {
    return this.apply('gray', text)
  }

  static brightRed (text) {
    return this.apply('brightRed', text)
  }

  static brightGreen (text) {
    return this.apply('brightGreen', text)
  }

  static brightYellow (text) {
    return this.apply('brightYellow', text)
  }

  static brightBlue (text) {
    return this.apply('brightBlue', text)
  }

  static brightMagenta (text) {
    return this.apply('brightMagenta', text)
  }

  static brightCyan (text) {
    return this.apply('brightCyan', text)
  }

  static brightWhite (text) {
    return this.apply('brightWhite', text)
  }
}
export { Color }
