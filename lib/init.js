import { ListenerLoader } from '../component/icqq/EventListener.js'
import Start from './API/model/watcher.js'
class Plugin {
  async init () {
    await new ListenerLoader().load(Bot)
  }

  initApi () {
    const api = new Start()
    api.setupWatcher()
  }
}

export default new Plugin()
